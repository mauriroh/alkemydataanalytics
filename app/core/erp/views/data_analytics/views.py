import pandas as pd
import os
import numpy as np

import requests

from datetime import datetime
from django.views.generic import CreateView, ListView, DetailView
from core.erp.models import Locality
from core.erp.forms import DataMCLModelForm
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.erp.models import DataMCL, Province, Department, Category, DataFile


class DataAnalyticsListView(ListView):
    model = DataMCL
    template_name = 'data_analytics/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in DataMCL.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado Categorías'
        context['download_url'] = reverse_lazy('app:filedownload')
        # context['list_url'] = reverse_lazy('productos_app:client-list')
        context['objet_list'] = DataMCL.objects.all()
        return context


class LibraryCreateView(CreateView):
    modelDataMCL = DataMCL
    modelProvince = Province
    modelDepartment = Department
    modelLocality = Locality
    modelCategory = Category
    modelDataFile = DataFile
    form_class = DataMCLModelForm
    template_name = 'data_analytics/librery.html'
    success_url = reverse_lazy('app:list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {'resp': True}
        '''Biblioteca'''
        pathFile = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQWvBZc_dhfkvL7JqRH6YtaJWqXUcguaTPbGpuw1TOtUgNlFK4aAdrqjdAfNgKDbw/pub?output=csv"
        try:
            data = pd.read_csv(pathFile)
            contador = 0

            for d in data.index:
                dataMCL = DataMCL()
                '''Control seguro reducción de errores.
                Convertir a tipo de datos para asegurar que la comparación
                entre los datos de entradas son del mismo tipo que los de la
                Base de Datos.
                '''
                cod_loc = int(data['Cod_Loc'][d])
                localidad = str(data['Localidad'][d])
                provincia = str(data['Provincia'][d])
                departamento = str(data['Departamento'][d])
                categoría = str(data['Categoría'][d])
                subcategoria = str(data['Subcategoria'][d])
                nombre = str(data['Nombre'][d])
                domicilio = str(data['Domicilio'][d])
                cp = str(data['CP'][d])
                teléfono = str(data['Teléfono'][d])
                mail = str(data['Mail'][d])
                web = str(data['Web'][d])
                download_date = datetime.now()

                '''Atributo Localidad
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if not (Locality.objects.filter(cod_locality__exact=cod_loc) and
                        Locality.objects.filter(name__icontains=localidad)):
                    local = Locality()
                    local.cod_locality = cod_loc
                    local.name = localidad
                    local.save()
                cod_loc2 = Locality.objects.filter(cod_locality__exact=cod_loc) and \
                       Locality.objects.filter(name__icontains=localidad)
                cod_locality = cod_loc2[0].id
                locality = cod_loc2[0].name

                '''Atributo Provincia
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdProvincia'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Province.objects.filter(cod_province__exact=cod_loc) and
                        Province.objects.filter(name__icontains=provincia)):
                    prov = Province()
                    prov.cod_province = cod_loc
                    prov.name = provincia
                    prov.save()
                cod_loc2 = Province.objects.filter(cod_province__exact=cod_loc) and \
                       Province.objects.filter(name__icontains=provincia)
                id_province = cod_loc2[0].id
                province = cod_loc2[0].name

                '''Atributo Department
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdDepartamento'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Department.objects.filter(cod_department__exact=cod_loc) and
                        Department.objects.filter(name__icontains=departamento)):
                    dep = Department()
                    dep.cod_department = cod_loc
                    dep.name = departamento
                    dep.save()
                cod_loc2 = Department.objects.filter(cod_department__exact=cod_loc) and \
                       Department.objects.filter(name__icontains=departamento)
                id_department = cod_loc2[0].id

                '''Atributo SubCategoria + Categoría + Nombre
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if subcategoria == 'nan' or subcategoria == 's/d' or subcategoria == ' ' or \
                        subcategoria == '' or subcategoria == None:
                    categoría = 'Bibliotecas' + ' - ' + categoría

                if not (Category.objects.filter(category__icontains=categoría) and
                        Category.objects.filter(name__icontains=nombre)):
                    cat = Category()
                    cat.category = categoría
                    cat.name = nombre
                    cat.save()
                cod_loc2 = Category.objects.filter(category__icontains=categoría) and \
                       Category.objects.filter(name__icontains=nombre)
                category = cod_loc2[0].id
                category_name = cod_loc2[0].name

                dataMCL.cod_locality_id = cod_locality
                dataMCL.id_province_id = id_province
                dataMCL.id_department_id = id_department
                dataMCL.category_id = category

                if province == 's/d' or province == None or province == 'nan':
                    dataMCL.province = ''
                else:
                    dataMCL.province = province
                if locality == 's/d' or locality == None or locality == 'nan':
                    dataMCL.locality = ''
                else:
                    dataMCL.locality = locality
                if category_name == 's/d' or category_name == None or category_name == 'nan':
                    dataMCL.category_name = ''
                else:
                    dataMCL.category_name = category_name
                if domicilio == 's/d' or domicilio == None or domicilio == 'nan':
                    dataMCL.address = ''
                else:
                    dataMCL.address = domicilio
                if cp == 's/d' or cp == None or cp == 'nan':
                    dataMCL.postcode = ''
                else:
                    dataMCL.postcode = cp
                if teléfono == 's/d' or teléfono == None or teléfono == 'nan':
                    dataMCL.phone = ''
                else:
                    dataMCL.phone = teléfono
                if mail == 's/d' or mail == None or mail == 'nan':
                    dataMCL.email = ''
                else:
                    dataMCL.email = mail
                if web == 's/d' or web == None or web == 'nan':
                    dataMCL.web = ''
                else:
                    dataMCL.web = web
                dataMCL.download_date = download_date
                dataMCL.save()
                '''Cantidad de Registros'''
                contador += 1

            dataFile = DataFile()
            dataFile.category_name = data['Categoría'][0]
            dataFile.total = int(contador)
            dataFile.date_file = datetime.now().strftime('%Y-%m-%d')
            dataFile.save()

            '''Crear archivo xlsx y csv.'''
            '''Anio en número | Mes en letras.'''
            year_month = datetime.now().strftime("%Y-%B")
            '''Fecha en números: Día - Mes - Anio.'''
            dates = datetime.now().strftime("%d-%m-%y")
            '''Categoría del Archivo.'''
            categories = data['Categoría'][0]
            '''Directorio Actual.'''
            path = os.getcwd()
            folder = os.path.join(path, 'folders')
            '''Ruta Destino'''
            path = f'\\{categories}\\{year_month}'
            new_path = folder + path
            file_name = f'{categories}-{dates}'
            '''Verificar existencia de carpeta'''
            if os.path.isdir(new_path):
                '''Reemplaza Archivo'''
                data.to_excel(new_path + '\\' + file_name + '.xlsx')
                data.to_csv(new_path + '\\' + file_name + '.csv')
            else:
                '''Crear nueva Carpeta | Crear nuevo Archivo.'''
                path = f'\\{categories}\\{year_month}'
                new_path = folder + path
                os.makedirs(new_path)
                os.chdir(new_path)
                os.getcwd()
                data.to_csv(file_name +'.csv')
                data.to_excel(file_name +'.xlsx')

        except Exception as e:
            response['error'] = str(e)
        finally:
            print(response)
        return JsonResponse(response, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Biblioteca'
        # context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


class MuseumsCreateView(CreateView):
    modelDataMCL = DataMCL
    modelProvince = Province
    modelDepartment = Department
    modelLocality = Locality
    modelCategory = Category
    modelDataFile = DataFile
    form_class = DataMCLModelForm
    template_name = 'data_analytics/museums.html'
    success_url = reverse_lazy('app:list')
    url_redirect = success_url


    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {'resp': True}
        '''Museo'''
        pathFile = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRhY5bVisEn_wZyY_m4Ub5lW1Ne2jW5BbYHRpNdzoJwRPuxt4UaF6niqVAbPtIJ_g/pub?output=csv"
        try:
            data = pd.read_csv(pathFile)
            contador = 0
            for d in data.index:
                dataMCL = DataMCL()
                '''Control seguro reducción de errores.
                Convertir a tipo de datos para asegurar que la comparación
                entre los datos de entradas son del mismo tipo que los de la
                Base de Datos.
                '''
                cod_loc = int(data['Cod_Loc'][d])
                localidad = str(data['localidad'][d])
                provincia = str(data['provincia'][d])
                departamento = ''
                categoría = str(data['categoria'][d])
                subcategoria = str(data['subcategoria'][d])
                nombre = str(data['nombre'][d])
                domicilio = str(data['direccion'][d])
                cp = str(data['CP'][d])
                teléfono = str(data['telefono'][d])
                mail = str(data['Mail'][d])
                web = str(data['Web'][d])
                download_date = datetime.now()

                '''Atributo Localidad
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if not (Locality.objects.filter(cod_locality__exact=cod_loc) and
                        Locality.objects.filter(name__icontains=localidad)):
                    local = Locality()
                    local.cod_locality = cod_loc
                    local.name = localidad
                    local.save()
                cod_loc2 = Locality.objects.filter(cod_locality__exact=cod_loc) and \
                       Locality.objects.filter(name__icontains=localidad)
                cod_locality = cod_loc2[0].id
                locality = cod_loc2[0].name

                '''Atributo Provincia
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdProvincia'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Province.objects.filter(cod_province__exact=cod_loc) and
                        Province.objects.filter(name__icontains=provincia)):
                    prov = Province()
                    prov.cod_province = cod_loc
                    prov.name = provincia
                    prov.save()
                cod_loc2 = Province.objects.filter(cod_province__exact=cod_loc) and \
                       Province.objects.filter(name__icontains=provincia)
                id_province = cod_loc2[0].id
                province = cod_loc2[0].name

                '''Atributo Department
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdDepartamento'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Department.objects.filter(cod_department__exact=cod_loc) and
                        Department.objects.filter(name__icontains=departamento)):
                    dep = Department()
                    dep.cod_department = cod_loc
                    dep.name = departamento
                    dep.save()
                cod_loc2 = Department.objects.filter(cod_department__exact=cod_loc) and \
                       Department.objects.filter(name__icontains=departamento)
                id_department = cod_loc2[0].id

                '''Atributo SubCategoria + Categoría + Nombre
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if subcategoria == 'nan' or subcategoria == 's/d' or subcategoria == ' ' or \
                        subcategoria == '' or subcategoria == None:
                    categoría = 'Bibliotecas' + ' - ' + categoría

                if not (Category.objects.filter(category__icontains=categoría) and
                        Category.objects.filter(name__icontains=nombre)):
                    cat = Category()
                    cat.category = categoría
                    cat.name = nombre
                    cat.save()
                cod_loc2 = Category.objects.filter(category__icontains=categoría) and \
                       Category.objects.filter(name__icontains=nombre)
                category = cod_loc2[0].id
                category_name = cod_loc2[0].name

                dataMCL.cod_locality_id = cod_locality
                dataMCL.id_province_id = id_province
                dataMCL.id_department_id = id_department
                dataMCL.category_id = category

                if province == 's/d' or province == None or province == 'nan' or province == '':
                    dataMCL.province = ''
                else:
                    dataMCL.province = province
                if locality == 's/d' or locality == None or locality == 'nan' or locality == '':
                    dataMCL.locality = ''
                else:
                    dataMCL.locality = locality
                if category_name == 's/d' or category_name == None or category_name == 'nan' or category_name == '':
                    dataMCL.category_name = ''
                else:
                    dataMCL.category_name = category_name
                if domicilio == 's/d' or domicilio == None or domicilio == 'nan' or domicilio == '':
                    dataMCL.address = ''
                else:
                    dataMCL.address = domicilio
                if cp == 's/d' or cp == None or cp == 'nan' or cp == '':
                    dataMCL.postcode = ''
                else:
                    dataMCL.postcode = cp
                if teléfono == 's/d' or teléfono == None or teléfono == 'nan' or teléfono == '':
                    dataMCL.phone = ''
                else:
                    dataMCL.phone = teléfono
                if mail == 's/d' or mail == None or mail == 'nan' or mail == '':
                    dataMCL.email = ''
                else:
                    dataMCL.email = mail
                if web == 's/d' or web == None or web == 'nan' or web == '':
                    dataMCL.web = ''
                else:
                    dataMCL.web = web
                dataMCL.download_date = download_date
                dataMCL.save()
                '''Cantidad de Registros'''
                contador += 1

            dataFile = DataFile()
            dataFile.category_name = data['categoria'][0]
            dataFile.total = int(contador)
            dataFile.date_file = datetime.now().strftime('%Y-%m-%d')
            dataFile.save()

            '''Crear archivo xlsx y csv.'''
            '''Anio en número | Mes en letras.'''
            year_month = datetime.now().strftime("%Y-%B")
            '''Fecha en números: Día - Mes - Anio.'''
            dates = datetime.now().strftime("%d-%m-%y")
            '''Categoría del Archivo.'''
            categories = data['categoria'][0]
            '''Directorio Actual.'''
            path = os.getcwd()
            folder = os.path.join(path, 'folders')
            '''Ruta Destino'''
            path = f'\\{categories}\\{year_month}'
            new_path = folder + path
            file_name = f'{categories}-{dates}'
            '''Verificar existencia de carpeta'''
            if os.path.isdir(new_path):
                '''Reemplaza Archivo'''
                data.to_excel(new_path + '\\' + file_name + '.xlsx')
                data.to_csv(new_path + '\\' + file_name + '.csv')
            else:
                '''Crear nueva Carpeta | Crear nuevo Archivo.'''
                path = f'\\{categories}\\{year_month}'
                new_path = folder + path
                os.makedirs(new_path)
                data.to_csv(file_name +'.csv')
                data.to_excel(file_name +'.xlsx')

        except Exception as e:
            response['error'] = str(e)
        finally:
            print(response)
        return JsonResponse(response, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Museos'
        # context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


class CinemasCreateView(CreateView):
    modelDataMCL = DataMCL
    modelProvince = Province
    modelDepartment = Department
    modelLocality = Locality
    modelCategory = Category
    modelDataFile = DataFile
    form_class = DataMCLModelForm
    template_name = 'data_analytics/cinemas.html'
    success_url = reverse_lazy('app:list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = {'resp': True}
        '''Museo'''
        pathFile = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQvKGxFd2YV8v7VYjNDu2w39X9ble8QmqK41CjJcs6DfWMv8Ul18y0JvmiDDjHTvg/pub?output=csv"
        try:
            data = pd.read_csv(pathFile)
            contador = 0
            for d in data.index:
                dataMCL = DataMCL()
                '''Control seguro reducción de errores.
                Convertir a tipo de datos para asegurar que la comparación
                entre los datos de entradas son del mismo tipo que los de la
                Base de Datos.
                '''
                cod_loc = int(data['Cod_Loc'][d])
                localidad = str(data['Localidad'][d])
                provincia = str(data['Provincia'][d])
                departamento = str(data['Departamento'][d])
                categoría = str(data['Categoría'][d])
                subcategoria = ''
                nombre = str(data['Nombre'][d])
                domicilio = str(data['Dirección'][d])
                cp = str(data['CP'][d])
                teléfono = str(data['Teléfono'][d])
                mail = str(data['Mail'][d])
                web = str(data['Web'][d])
                download_date = datetime.now()

                '''Atributo Localidad
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if not (Locality.objects.filter(cod_locality__exact=cod_loc) and
                        Locality.objects.filter(name__icontains=localidad)):
                    local = Locality()
                    local.cod_locality = cod_loc
                    local.name = localidad
                    local.save()
                cod_loc2 = Locality.objects.filter(cod_locality__exact=cod_loc) and \
                       Locality.objects.filter(name__icontains=localidad)
                cod_locality = cod_loc2[0].id
                locality = cod_loc2[0].name

                '''Atributo Provincia
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdProvincia'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Province.objects.filter(cod_province__exact=cod_loc) and
                        Province.objects.filter(name__icontains=provincia)):
                    prov = Province()
                    prov.cod_province = cod_loc
                    prov.name = provincia
                    prov.save()
                cod_loc2 = Province.objects.filter(cod_province__exact=cod_loc) and \
                       Province.objects.filter(name__icontains=provincia)
                id_province = cod_loc2[0].id
                province = cod_loc2[0].name

                '''Atributo Department
                Variable aux para convertir a entero antes de comparar con el atributo en la BD.
                Nos aseguramos que la comparación sea entre variables enteras
                '''
                aux = data['IdDepartamento'][d]
                '''Se verifica si existe en tabla, sino existe se da de alta.'''
                if not (Department.objects.filter(cod_department__exact=cod_loc) and
                        Department.objects.filter(name__icontains=departamento)):
                    dep = Department()
                    dep.cod_department = cod_loc
                    dep.name = departamento
                    dep.save()
                cod_loc2 = Department.objects.filter(cod_department__exact=cod_loc) and \
                       Department.objects.filter(name__icontains=departamento)
                id_department = cod_loc2[0].id

                '''Atributo SubCategoria + Categoría + Nombre
                Se verifica si existe en tabla, sino existe se da de alta.
                '''
                if subcategoria == 'nan' or subcategoria == 's/d' or subcategoria == ' ' or \
                        subcategoria == '' or subcategoria == None:
                    categoría = 'Bibliotecas' + ' - ' + categoría

                if not (Category.objects.filter(category__icontains=categoría) and
                        Category.objects.filter(name__icontains=nombre)):
                    cat = Category()
                    cat.category = categoría
                    cat.name = nombre
                    cat.save()
                cod_loc2 = Category.objects.filter(category__icontains=categoría) and \
                       Category.objects.filter(name__icontains=nombre)
                category = cod_loc2[0].id
                category_name = cod_loc2[0].name

                dataMCL.cod_locality_id = cod_locality
                dataMCL.id_province_id = id_province
                dataMCL.id_department_id = id_department
                dataMCL.category_id = category

                if province == 's/d' or province == None or province == 'nan' or province == '':
                    dataMCL.province = ''
                else:
                    dataMCL.province = province
                if locality == 's/d' or locality == None or locality == 'nan' or locality == '':
                    dataMCL.locality = ''
                else:
                    dataMCL.locality = locality
                if category_name == 's/d' or category_name == None or category_name == 'nan' or category_name == '':
                    dataMCL.category_name = ''
                else:
                    dataMCL.category_name = category_name
                if domicilio == 's/d' or domicilio == None or domicilio == 'nan' or domicilio == '':
                    dataMCL.address = ''
                else:
                    dataMCL.address = domicilio
                if cp == 's/d' or cp == None or cp == 'nan' or cp == '':
                    dataMCL.postcode = ''
                else:
                    dataMCL.postcode = cp
                if teléfono == 's/d' or teléfono == None or teléfono == 'nan' or teléfono == '':
                    dataMCL.phone = ''
                else:
                    dataMCL.phone = teléfono
                if mail == 's/d' or mail == None or mail == 'nan' or mail == '':
                    dataMCL.email = ''
                else:
                    dataMCL.email = mail
                if web == 's/d' or web == None or web == 'nan' or web == '':
                    dataMCL.web = ''
                else:
                    dataMCL.web = web
                dataMCL.download_date = download_date
                dataMCL.save()
                '''Cantidad de Registros'''
                contador += 1

            dataFile = DataFile()
            dataFile.category_name = data['Categoría'][0]
            dataFile.total = int(contador)
            dataFile.date_file = datetime.now().strftime('%Y-%m-%d')
            dataFile.save()

            '''Crear archivo xlsx y csv.'''
            '''Anio en número | Mes en letras.'''
            year_month = datetime.now().strftime("%Y-%B")
            '''Fecha en números: Día - Mes - Anio.'''
            dates = datetime.now().strftime("%d-%m-%y")
            '''Categoría del Archivo.'''
            categories = data['Categoría'][0]
            '''Directorio Actual.'''
            path = os.getcwd()
            folder = os.path.join(path, 'folders')
            '''Ruta Destino'''
            path = f'\\{categories}\\{year_month}'
            new_path = folder + path
            file_name = f'{categories}-{dates}'
            '''Verificar existencia de carpeta'''
            if os.path.isdir(new_path):
                '''Reemplaza Archivo'''
                data.to_excel(new_path + '\\' + file_name + '.xlsx')
                data.to_csv(new_path + '\\' + file_name + '.csv')
            else:
                '''Crear nueva Carpeta | Crear nuevo Archivo.'''
                path = f'\\{categories}\\{year_month}'
                new_path = folder + path
                os.makedirs(new_path)
                data.to_csv(file_name +'.csv')
                data.to_excel(file_name +'.xlsx')

        except Exception as e:
            response['error'] = str(e)
        finally:
            print(response)
        return JsonResponse(response, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Salas de Cines'
        context['action'] = 'add'
        context['list_url'] = 'data_analytics/list.html'
        return context


class DataFileListView(ListView):
    model = DataFile
    template_name = 'data_analytics/datafile.html'
    success_url = reverse_lazy('app:list')
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in DataFile.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Data Analytics'
        cont_libreries = 0
        cont_cinemas = 0
        cont_museums = 0
        for i in DataFile.objects.all():
            if i.category_name.__contains__('Bibliotecas'):
                print(i.total.__int__())
                cont_libreries = cont_libreries + i.total.__int__()
            elif i.category_name.__contains__('cine'):
                cont_cinemas = cont_cinemas + i.total.__int__()
            else:
                cont_museums = cont_museums + i.total.__int__()
        # context['datafile'] = DataFile.objects.all()
        # context['nav_productions'] = 'Producción'
        # context['nav_tiendanube_api'] = 'Tiendanube API'
        # context['nav_sale'] = 'Ventas'
        context['cont_libreries'] = cont_libreries
        context['cont_cinemas'] = cont_cinemas
        context['cont_museums'] = cont_museums
        context['list_url'] = reverse_lazy('app:list')
        return context


