from django.urls import path
from core.erp.views.data_analytics.views import *

app_name = 'app'

urlpatterns = [
    path('data_analytics/list/', DataAnalyticsListView.as_view(), name='list'),
    path('data_analytics/librery/', LibraryCreateView.as_view(), name='librery'),
    path('data_analytics/museums/', MuseumsCreateView.as_view(), name='museums'),
    path('data_analytics/cinemas/', CinemasCreateView.as_view(), name='cinemas'),
    path('data_analytics/datafile/', DataFileListView.as_view(), name='datafile'),
]