from django.contrib import admin
from core.erp.models import *

#Register your models here.
admin.site.register(Province)

admin.site.register(Department)

admin.site.register(Locality)

admin.site.register(Category)

admin.site.register(DataMCL)

admin.site.register(DataFile)