from django.db import models
from django.forms import model_to_dict
from datetime import datetime
from app.settings import MEDIA_URL, STATIC_URL


# Create your models here.
# Model Provincias
class Province(models.Model):
    cod_province = models.IntegerField(default=0, verbose_name='Código Provincia')
    name = models.CharField(max_length=250, blank=True, default='', verbose_name='Provincia')

    def __str__(self):
        return self.cod_province

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Provincia'
        verbose_name_plural = 'Provincias'
        db_table = 'province'
        ordering = ['cod_province']


# Model Departamentos
class Department(models.Model):
    cod_department = models.IntegerField(default=0, verbose_name='Código Departamento')
    name = models.CharField(max_length=250, blank=True, default='', verbose_name='Departamento')

    def __str__(self):
        return self.cod_department

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        db_table = 'department'
        ordering = ['cod_department']


# Model Localidad
class Locality(models.Model):
    cod_locality = models.IntegerField(default=0, verbose_name='Código Localidad')
    name = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')

    def __str__(self):
        return self.cod_locality

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Localidad'
        verbose_name_plural = 'Localidades'
        db_table = 'locality'
        ordering = ['cod_locality']


# Model Categoría
class Category(models.Model):
    category = models.CharField(max_length=250, blank=True, default='', verbose_name='Categoría')
    name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre Categoría')

    def __str__(self):
        return self.category

    def toJSON(self):
        item = model_to_dict(self)
        return item
    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        db_table = 'category'
        ordering = ['category']


# Model Museum | Cinemas | Library
class DataMCL(models.Model):
    cod_locality = models.ForeignKey(Locality, on_delete=models.CASCADE)
    id_province = models.ForeignKey(Province, on_delete=models.CASCADE)
    id_department = models.ForeignKey(Department, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    province = models.CharField(max_length=250, blank=True, default='', verbose_name='Provincia')
    locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
    category_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre Categoría')
    address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
    postcode = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
    phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Localidad')
    email = models.EmailField(blank=True, default='', verbose_name='Email')
    web = models.CharField(max_length=1000, blank=True, default='', verbose_name='Web')
    download_date = models.DateField(default=datetime.now)

    def __str__(self):
        return self.category.category

    def toJSON(self):
        item = model_to_dict(self)
        item['cod_locality'] = self.cod_locality.toJSON()
        item['id_province'] = self.id_province.toJSON()
        item['id_department'] = self.id_department.toJSON()
        item['category'] = self.category.toJSON()
        item['download_date'] = self.download_date.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'DatosMCL'
        verbose_name_plural = 'DatosMCL'
        db_table = 'datamcl'
        ordering = ['category']


class DataFile(models.Model):
    category_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre Categoría')
    total = models.IntegerField(default=0)
    date_file = models.DateField(default=datetime.now)

    def __str__(self):
        return self.category_name

    def toJSON(self):
        item = model_to_dict(self)
        item['date_file'] = self.date_file.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'Archivo de Dato'
        verbose_name_plural = 'Archivo de Datos'
        db_table = 'datafile'
        ordering = ['category_name']
